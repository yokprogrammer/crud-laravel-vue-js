<p align="center"><img src="https://cdn-images-1.medium.com/max/1200/1*FPKwlbwrWYmQ49FmMXu4Ug.png" width="150"></p>

# CRUD Laravel + Vue Js

> Example 5.7, news features to development

## Download
First, clone project:
``` bash
# clone
git clone https://gitlab.com/yokprogrammer/crud-laravel-vue-js

# Access project
cd crud-laravel-vue-js
```

## Config

``` bash
# Install dependencies
composer install

# Create file .env
cp .env.example .env

# Generate key
php artisan key:generate

# Run migrations (tables and Seeders)
php artisan migrate --seed

# Create Server
php artisan serve

# Access project
http://localhost:8080

# List Data
http://localhost:8080/users

# Install npm
npm install

# Install cross-env
npm install --global cross-env
npm install --no-bin-links

# Run vue
npm run watch
```